# Tally Counter

### Series 4

Implement all functions in src/main/java/sbu/cs/exercises/*.java
regarding the guidelines commented before any function.  

In TallyCounter.java class, you should throw an exception

<details>
<summary>More about exceptions in Java</summary>

An exception is an unwanted or unexpected event, which occurs during the execution of a program i.e at run time,
that disrupts the normal flow of the program’s instructions.

to throw an exception do as follows
```java
//throw new ExceptionClass();
```
where ExceptionClass can be Exception or IllegalValueException or RuntimeException or ...

There are many built-in exception classes in Java that you can use,
but you can make your own custom Exception by making a class
and extending the Exception class.

<a href="https://www.tutorialspoint.com/java/java_exceptions.htm">
Read this article for more information
</a>
or ask your mentors about exceptions.
</details>


* all questions are from lectures 4 to 7. you can find the original questions there.

* right after forking add your mentor to project as maintainer and after that create a branch
in this format \<feature or bugfix>/<what-you-want-to-do-in-this-branch> and start coding.   
  branch name exp: feature/lecture-4-exercises 

* set a safe deadline for yourself. your mentor may not accept your merge request.  
for this break parts of codes and create branch for each part and send merge request
  for each part separately. Then wait for the review of your mentor.
  Don't make a merge request unless your implementations passes all the test cases.
  If your merge request is merged, you're done; Otherwise, Change your code according 
  to the comments your mentor writes for you and then push your new code and request 
  a review again.
  
**Good luck with your exercises**
