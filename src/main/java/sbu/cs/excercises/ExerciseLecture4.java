package sbu.cs.excercises;

import sbu.cs.IllegalValueException;

import java.util.ArrayList;
import java.util.HashMap;

public class ExerciseLecture4 {

    /**
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     * @exception IllegalValueException when the factorial of n doesn't exist
     * @param n an integer which the function should return its factorial, namely
     *          n!
     */
    public long factorial(int n) throws IllegalValueException
    {
        if(n < 0)throw new IllegalValueException();
        else {
            long temp = 1;
            for (int i = 1; i <= n; i++) {
                temp = temp * i;
            }
            return temp;
        }
    }

    /**
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     * @param n an integer that indicated the nth number in fibonacci series
     */
    public long fibonacci(int n)
    {
        long n1=0,n2=1,n3,i;
        // System.out.print(n1+" "+n2);//printing 0 and 1
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.add(n1);
        arrayList.add(n2);

        for(i=2;i<=n;++i)//loop starts from 2 because 0 and 1 are already printed
        {
            n3=n1+n2;
            //   System.out.print(" "+n3);
            arrayList.add(n3);
            n1=n2;
            n2=n3;
        }
        return (arrayList.get(n));

    }

    /**
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     * @param word the word as a string
     */
    public String reverse(String word)
    {
        String reverse = new StringBuffer(word).reverse().toString();
        return reverse;
    }

    /**
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     *
     * @param line the line of text as a string
     */
    public boolean isPalindrome(String line)
    {
        boolean output=false;
        line  = line.replace(" ", "");
        line = line.toLowerCase();
        String reverse = new StringBuffer(line).reverse().toString();
        if(line.equals(reverse))output=true;
        return output;
    }

    /**
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and hello is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     * @param string1 the first string
     * @param string2 the second string
     */
    public char[][] dotPlot(String string1, String string2)
    {
        int s1Len = string1.length();
        int s2Len = string2.length();
        char[] char1 = string1.toCharArray();
        char[] char2 = string2.toCharArray();
        char [][] dotPlot = new  char[s1Len][s2Len];

        int tempI = 0;
        int tempJ = 0;

        for (int i = 0; i < s1Len; i++)
        {
            tempI = i;
            for (int j = 0; j < s2Len; j++)
            {

                if(char1[i] == char2[j])
                {
                    dotPlot[i][j] = '*';
                }
                else dotPlot[i][j] =' ';

            }

        }
        for (int i = 0; i < s1Len; i++)
        {
            for (int j = 0; j < s2Len; j++)
            {
                System.out.print(dotPlot[i][j] + " ");
                if(j == s1Len-1) System.out.println();
            }

        }
        return dotPlot;
    }
}
