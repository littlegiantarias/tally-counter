package sbu.cs.excercises;

import sbu.cs.IllegalValueException;

import javax.management.StringValueExp;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExerciseLecture6 {

    /**
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     * @param array an integer array
     */
    public long calculateEvenSum(int[] array)
    {
        long sum = 0;
        for (int i = 0; i < array.length; i+=2)
        {
            sum = sum + array[i];
        }
        return sum;
    }

    /**
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     * @param array an integer array
     */
    public int[] reverseArray(int[] array)
    {
        int[] b = new int[array.length];
        int j = array.length;
        for (int i = 0; i < array.length; i++) {
            b[j - 1] = array[i];
            j = j - 1;
        }
        return b;

    }

    /**
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     * @param matrix1 the first matrix
     * @param matrix2 the second matrix
     */
    public double[][] matrixProduct(double[][] matrix1, double[][] matrix2) throws RuntimeException
    {
        int row1 = matrix1.length;
        int col1 = matrix1[0].length;

        int row2 = matrix2.length;
        int col2 = matrix2[0].length;
        double prod[][] = new double[row1][col2];

        if(col1 != row2){
            throw new IllegalValueException();
        }
        else{
            for(int i = 0; i < row1; i++){
                for(int j = 0; j < col2; j++){
                    for(int k = 0; k < row2; k++){
                        prod[i][j] = (prod[i][j] + matrix1[i][k] * matrix2[k][j]);
                    }
                }
            }



        }

        return prod;
    }

    /**
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     * @param names a two-dimensional array of strings
     */
    public List<List<String>> arrayToList(String[][] names)
    {
       return null;
    }

    /**
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     * @param n the number n
     */
    public List<Integer> primeFactors(int n)
    {
        ArrayList<Integer> ans1 = new ArrayList<>();
        int number;
        number = n;
        for(int i = 2; i< number; i++)
        {
            while(number%i == 0)
            {
                ans1.add(i);
                number = number/i;
            }
        }
        if(number >2) {
            ans1.add(number);
        }
        ArrayList<Integer> newList = new ArrayList<>();

        // Traverse through the first list
        for (Integer element : ans1) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }
       return newList;

    }

    /**
     *   implement a function that return a list of words in a given string.
     *   consider that words are separated by spaces, commas,questions marks....
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {

        List<String> finalWords = new ArrayList<String>();
        String[] splited = line.split("( |\\!|\\?|\\,)+");
        for (String c : splited) {
            finalWords.add(c);
        }
        return finalWords;

    }


}
