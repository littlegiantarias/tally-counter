package sbu.cs.excercises;

import java.util.ArrayList;
import java.util.Random;

public class ExerciseLecture5 {

    /**
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String weakPassword(int length)
    {
        StringBuilder password = new StringBuilder();
        int max = 122;
        int min = 97;
        for (int i = 0; i < length; i++)
        {
            int v = (int) (Math.floor(Math.random() * (max - min + 1)) + min);
            password.append(Character.toString((char) v));
        }
        return String.valueOf(password);
    }

    /**
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String strongPassword(int length)
    {

            Random rnd = new Random();
            String specialCharacters = "!@#$%^&*";
            String digits = "1234567890";
            String allphabet = "abcdefghijklmnopqrstuvwxyz";
            StringBuilder password = new StringBuilder();
            int min = 1;
            int max = length - 3;
            Random random = new Random();
            int digitNum = random.nextInt(max + min) + min;
            int minChar=1;
            int maxChar=length-digitNum-2;
            int charNum = random.nextInt(maxChar + minChar) + minChar;
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < digitNum; i++)
                sb.append(specialCharacters.charAt(rnd.nextInt(specialCharacters.length())));
            for (int i = 0; i < charNum; i++) sb.append(digits.charAt(rnd.nextInt(digits.length())));

            int allph = length - (charNum + digitNum);
            for (int i = 0; i < allph; i++) sb.append(allphabet.charAt(rnd.nextInt(allphabet.length())));
            return sb.toString();


    }

    /**
     *   implement a function that checks if an integer is a fibobin number
     *   integer n is fibobin if there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     * @param n the number n
     */
    public boolean isFiboBin(int n)
    {
        int n1=0,n2=1,n3,i;
        // System.out.print(n1+" "+n2);//printing 0 and 1
        ArrayList<Integer> arrayList = new ArrayList<>();
        ArrayList<Integer> fibobin   = new ArrayList<>();
        arrayList.add(n1);
        arrayList.add(n2);
        for(i=2;i<=n;++i)//loop starts from 2 because 0 and 1 are already printed
        {
            n3=n1+n2;
            arrayList.add(n3);
            n1=n2;
            n2=n3;
        }

        arrayList.remove(0);
        System.out.println(arrayList);

        int count = 0;
        for (int j = 0; j < n ; j++)
        {
            String binary = Integer.toBinaryString(arrayList.get(j));
            for (int k = 0; k < binary.length(); k++) {
                if (binary.charAt(k) == '1')
                    count++;
            }
            fibobin.add(arrayList.get(j) + count);

        }
        fibobin.remove(1);
        return fibobin.contains(n);


    }
}
